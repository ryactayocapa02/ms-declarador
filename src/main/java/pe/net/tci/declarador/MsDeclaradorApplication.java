package pe.net.tci.declarador;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsDeclaradorApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsDeclaradorApplication.class, args);
	}

}
