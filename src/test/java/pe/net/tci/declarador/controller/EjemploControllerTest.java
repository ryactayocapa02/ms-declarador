package pe.net.tci.declarador.controller;

		import org.junit.jupiter.api.Test;
		import org.junit.jupiter.api.extension.ExtendWith;
		import org.springframework.beans.factory.annotation.Autowired;
		import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
		import org.springframework.boot.test.mock.mockito.MockBean;
		import org.springframework.context.annotation.Bean;
		import org.springframework.test.context.ContextConfiguration;
		import org.springframework.test.context.junit.jupiter.SpringExtension;
		import org.springframework.test.web.servlet.MockMvc;
		import pe.net.tci.declarador.service.EjemploService;

		import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
		import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@ExtendWith(SpringExtension.class)
@WebMvcTest(EjemploController.class)
@ContextConfiguration(classes = {EjemploControllerTest.Configuration.class})
public class EjemploControllerTest {

	public static class Configuration{

		@Bean
		public EjemploController DeclaradorController(EjemploService ejemploService){
			return new EjemploController(ejemploService);
		}

	}

	@MockBean
	private EjemploService ejemploService;

	@Autowired
	private MockMvc mockMvc;

	@Test
	void enviarEjemplo_ok() throws Exception {
		// Preparing data
		// Mocks & Stubs configuration
		// Business logic execution
		// Validating mocks behaviour
		// Validating results
	}


}
