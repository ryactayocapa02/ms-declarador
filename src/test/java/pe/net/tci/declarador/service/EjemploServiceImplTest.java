package pe.net.tci.declarador.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import pe.net.tci.declarador.repository.EjemploRepository;
import java.util.Arrays;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {EjemploServiceImplTest.Configuration.class})
public class EjemploServiceImplTest{

static class Configuration {

    @Bean
    public EjemploService ejemploService(EjemploRepository ejemploRepository) {
        return new EjemploServiceImpl(ejemploRepository);
    }
}

    @MockBean
    private EjemploRepository ejemploRepository;


    @Autowired
    private EjemploService ejemploService;

    @BeforeEach
    void cleanCachesAndMocksAndStubs(){
        reset(ejemploRepository);
    }


    @Test
    public void save_ok() {
        // Preparing data
        // Mocks & Stubs configuration
        // Business logic execution
        // Validating mocks behaviour
        // Validating results
    }


}
